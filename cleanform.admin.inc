<?php
function cleanform_admin_settings_form(){
  $form['cleanforms'] = array(
    '#title' => t(''),
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $roles = user_roles();
  $form['cleanforms']['cleanform_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#default_value' => variable_get('cleanform_roles', array('')),
    '#options' => $roles,
    '#description' => t(''),
  );

  $fieldsets = cleanform_node_options('page');
  
  foreach($fieldsets as $name => $fieldset) {
    $fields[$name]=$fieldset['#title'];
  }

  $form['cleanforms']['cleanform_fields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Fields to be excluded'),
    '#default_value' => variable_get('cleanform_fields', array('')),
    '#options' => $fields,
    '#description' => t(''),
  );

  $content = node_get_types();
  
  foreach($content as $value) {
    $content_type[$value->type]=$value->name;
  }

  $form['cleanforms']['cleanform_contents'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content type'),
    '#default_value' => variable_get('cleanform_contents', array('')),
    '#options' => $content_type,
    '#description' => t(''),
  );

  return system_settings_form($form);
}


function cleanform_node_options($node_type) {
  module_load_include('inc', 'node', 'node.pages');
  $node = (object) array(
    'type' => $node_type,
    'name' => '',
    'vid' => 0,
    'language' => '',
  );

  // Build a sample node form to find fieldsets.
  $node_form_state = array('values' => array());
  $node_form_id = $node_type .'_node_form';
  $args = array(
    $node_form_id,
    $node_form_state,
    $node
  );
  $node_form = call_user_func_array('drupal_retrieve_form', $args);
  $node_form['#post'] = $node_form_state['values'];
  drupal_prepare_form($node_form_id, $node_form, $node_form_state);
  uasort($node_form, 'element_sort');
  return cleanform_get_form_elements($node_form);
}




/**
 * Get all the fieldset elements from a form.
 */
function cleanform_get_form_elements(&$form) {
  $elements = array();
  foreach (element_children($form) as $key) {
    if (!isset($form[$key]['#type'])) {
      continue;
    }
    elseif (!in_array($form[$key]['#type'], array('fieldset'))) {
      continue;
    }
    elseif (isset($form[$key]['#access']) && !$form[$key]['#access']) {
      continue;
    }
    $elements[$key] = &$form[$key];
  }
  return $elements;
}

